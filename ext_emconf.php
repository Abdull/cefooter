<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "cefooter".
 *
 * Auto generated 14-11-2020 12:42
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Content element footer info',
  'description' => 'Preview of settings in footer of tt_content element in page module.',
  'category' => 'be',
  'version' => '1.3.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' =>
  array (
    'depends' =>
    array (
      'typo3' => '10.4.0-10.4.99',
    ),
    'conflicts' =>
    array (
    ),
    'suggests' =>
    array (
    ),
  ),
);

