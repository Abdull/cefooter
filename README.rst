=============
Documentation
=============

----------------
What does it do?
----------------

This extension shows informations of a content element in the footer of the content element in TYPO3 BE in the page module:

.. figure:: Documentation/screenshot1.png


It's configurable with Page TSconfig, which informations are shown and under what conditions, e.g. the field "layout" is only shown when layout is not "none" or "default", or field "subheader" is only shown when the content type is "textpic".


-------------
Configuration
-------------


Add Page TSconfig
=================

Add the predefined Page TSconfig by selecting item "EXT:cefooter: Preview settings" in your ROOT page or write your own Page TSconfig.


Configuration
=============

Configure the fields which should be shown in the content element footer:

::

    mod.web_layout.tt_content_footer.show = frame_class, layout, subheader


Configure the conditions for showing a field in the content element footer:

::

    mod.web_layout.tt_content_footer.displayCond {
        frame_class {
            10 {
                valuesNot = none,default
            }
        }
        subheader {
            10 {
                field = CType
                values = textpic
            }
        }
    }


Problems
========

Field names and values doesn't match
******************************************

For some fields it could happen that the field names and field values you see in the page properties doesn't match the names and values you see in the content element footer.
This is an issue of TYPO3 CMS, because the internal function which builds the content element footer just reads the informations from TCA.
If this bothers you, you have to overwrite the TCA setting...

::

    $GLOBALS['TCA']['tt_content']['columns']['subheader']['label'] = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:subheader_formlabel';

...or overwrite the language file:

::
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:frontend/Resources/Private/Language/locallang_ttc.xlf'][] = 'EXT:yourExt/Resources/Private/Language/Overrides/locallang_ttc.xlf';


----
Todo
----

- Find a better solution for configuration of display conditions.
- Show settings from FlexForms


---
FAQ
---

Usage with TYPO3 < 10
=====================

Please use version 1.1.0 from TER.


---------
ChangeLog
---------

See file **ChangeLog** in the extension directory.
